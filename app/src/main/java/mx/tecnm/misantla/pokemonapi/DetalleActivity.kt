package mx.tecnm.misantla.pokemonapi

import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import mx.tecnm.misantla.pokemonapi.databinding.ActivityDetalleBinding
import mx.tecnm.misantla.pokemonapi.model.Pokemon

class DetalleActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetalleBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetalleBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val pokemon = intent.getSerializableExtra("poke") as Pokemon

        if(pokemon != null) {
            Glide.with(this)
                .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemon.number}.png")
                .into(binding.ivDetalleImagen)
            val font = Typeface.createFromAsset(assets, "product_sans_bold.ttf")
            binding.tvDetalleNombre.typeface = font

            binding.tvDetalleNombre.text = pokemon.name

        }
    }
}